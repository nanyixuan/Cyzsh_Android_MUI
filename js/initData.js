var url_primarypath = "http://upday.ecst.com.cn:81/cyzsh/api";
//var url_primarypath = "http://192.168.3.57:81/cyzsh/cyzsh/api";

//获取头条
var url_headlines = url_primarypath + "/newest_headlines.do";
//获得新闻列表
var url_getnews = url_primarypath + "/articles.do?type=news";
//获得文章详情
var url_getnews_xiangqing = url_primarypath + "/article_content.do"
	//登陆
var url_login = url_primarypath + "/login.do";
//获取公司列表
var url_getcompany = url_primarypath + "/thumbnail_supporters.do?filter=elec";
//获取公司详情
var url_getcompany_info = url_primarypath + "/supporter/details.do";
//获取用户收藏
var url_get_user_favorite = url_primarypath + "/collections.do";
//获得政策列表
var url_get_policy = url_primarypath + "/articles.do?type=polices";
//获得活动列表
var url_get_activity = url_primarypath + "/articles.do?type=activities";
var url_getcompany_info = url_primarypath + "/supporter/details.do";
//获取评论
var url_supporter_comments = url_primarypath + "/supporter/comments.do"
//提交评论
var url_supporter_comment = url_primarypath + "/supporter/comment.do"

var isLogin = false;
var c = "";
var d = "";

function login(username, pwd) {
	plus.nativeUI.showWaiting("数据加载中...");
	mui.ajax(url_login, {
		data: {
			"username": username,
			"password": pwd
		},
		type: 'post', //HTTP请求类型
		timeout: 10000, //超时时间设置为10秒；
		success: function(data) {
			//服务器返回响应，根据响应结果，分析是否登录成功；
			console.log(data);
			var json_value = JSON.parse(data);
			if(json_value.status == "200") {
				var body = json_value.value
				console.log(body.name);
				plus.storage.setItem("nickname", body.name);
				plus.webview.getWebviewById("examples/my_page.html").reload();
				plus.webview.currentWebview().close();
				mui.ajax(url_get_user_favorite, {
					type: 'get', //HTTP请求类型
					timeout: 10000, //超时时间设置为10秒；
					success: function(datas) {
						//服务器返回响应，根据响应结果，分析是否登录成功；
						console.log(JSON.stringify(JSON.parse(datas)));
						var json_value = JSON.parse(datas);
					},
					error: function(xhr, type, errorThrown) {
						//异常处理；
						console.log(type);
						plus.nativeUI.closeWaiting();
					}
				});
			} else {
				plus.nativeUI.toast(json_value.msg);
			}
			plus.nativeUI.closeWaiting();
		},
		error: function(xhr, type, errorThrown) {
			//异常处理；
			console.log(type);
			console.log(errorThrown);
			plus.nativeUI.closeWaiting();
		}
	});
}

function removeHTMLTag(str) {
	str = str.replace(/<\/?[^>]*>/g, ''); //去除HTML tag
	str = str.replace(/[ | ]*\n/g, '\n'); //去除行尾空白
	//str = str.replace(/\n[\s| | ]*\r/g,'\n'); //去除多余空行
	str = str.replace(/ /ig, ''); //去掉 
	return str;
}

function tagsToChinese(tagsname) {
	var value = "";
	switch(tagsname) {
		case "dzxx":
			value = "电子信息";
			break;
		case "hlwyydhlw":
			value = "互联网与移动互联网";
			break;
		case "kjfwy":
			value = "科技服务业";
			break;
		case "swyy":
			value = "生物医药";
			break;
		case "xcl":
			value = "新材料";
			break;
		case "xjzz":
			value = "先进制造";
			break;
		case "xnyjjnhb":
			value = "新能源及节能环保";
			break;
	}
	return value
}